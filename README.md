# An affordable open-source toolkit to discover interactive deformable surfaces

Here are instructions on how to assemble the toolkit and run the demo.

# Hardware

## 3d printing

3d print the following files:
- [Base STL file](hardware/FaderAssembly%20-%20Base.STL)
- [Holder STL file](hardware/FaderAssembly%20-%20Holder.STL)
- [Slider STL file](hardware/FaderAssembly%20-%20Slid.STL)
- [Top cover STL file](hardware/AssemblySQUARE_-_SQUAREtopSurface.STL)
- [Walls STL file](hardware/FaderAssembly%20-%20WallsForFabric.STL)
- [Frame for fabric STL file](hardware/AssemblySQUARE_-_SQUAREtopFrame.STL)
- [Esp32 thing plus holder STL file](hardware/FaderAssembly%20-%20Esp32ThingsPLusHolder.STL)

You can find the 3d-printing configuration files (with pieces pre-placed).
For each assembly, you'll need to print each of those
 
- [Part 1](hardware/Part1.3mf)
- [Part 2](hardware/Part2.3mf)
- [Part 3](hardware/Part3.3mf)

However, some pieces will be missing: so you will need to print these files once per 3 assemblies:
- [Part A](hardware/PartACommon_need_to_print_only_per_3_assemblies_.3mf)
- [Part B](hardware/PartBCommon_need_to_print_only_per_3_assemblies__ConductivePLA.3mf)


### Electronics
 
This is the schematic of the [electrical connections to make for this setup](hardware/Schematic_H-Bridge_controlling_4_firefaders_on_an_Esp32_2022-06-10__1_.pdf)

Requirements: 
- 2 [L298N H-Bridges](https://www.amazon.ca/PChero-Controller-Module-Stepper-Arduino/dp/B07GTCWN9Z/ref=asc_df_B07GTCWN9Z/?tag=googleshopc0c-20&linkCode=df0&hvadid=459480626555&hvpos=&hvnetw=g&hvrand=10189234331952644688&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9061023&hvtargid=pla-652109211542&psc=1) controlled by PWM signals sent by the Esp32.
- 4 [100mm Motor-driven Master Type with Touch sensor from Mouser](https://www.mouser.ca/ProductDetail/688-RSA0N11M9A0J). 

Please note that the connections are different from what the data sheet provides. Here are the observed pins for this type of fader.
![Fader Pins Mouser](hardware/MouserFaderDiagram.jpg)


## Assembly

- Gather all the pieces you need (we will show here how to assemble the system for 1 fader): 
  - the 3d-printed base 
  - the holder (1 for each fader)
  - the rod (1 for each fader)
  - 8mm M3 bolts (4 for each fader)
  - M3 nuts (4 for each fader)
  - 5mm M3 bolts (2 for each fader)
  - Fader
- Isolate the rod and the holder. Slide the rod into the channel of the holder. 
- Make sure that the groove (in which the fader is going to clip) of the rod  is aligned with the groove of the holder.
- Now isolate the base and the four nuts, and place the four nuts in the appropriate grooves.
- Then flip the base (you may need to use a piece of paper placed on the base to prevent the nuts from falling when flipping).
- Align the holder with the holes in which the nuts are, and secure the holder to the base using the 8mm M3 bolts.
- Align the fader with the rod groove and press it into this groove.
- Align the fader mounting holes with the holder mounting holes and secure those two pieces together using the two 5mm M3 bolts.
- Secure the top cover to the holders using M3 5mm Bolts and M3 nuts.
- Optionally secure walls on the top cover using M3 5mm Bolts and nuts.
- Optionally secure the top frame to those walls using M3 5mm Bolts and nuts and glue the fabric on that frame.
- Follow the electronics diagram and make the appropriate connections. 
- Then slide the Esp32 things plus into its corresponding bracket and secure it using 5mm M3 bolts.
- Then screw the bottom cover to protect the electronics.

Here is what the assembly looks like in an exploded view.
![image of assembly exploded](hardware/AssemblyEclateeLegende.jpg)

Here is what the assembly should look like.
![image of assembly ](hardware/Assembly.JPG)

If you want to have a 3d look at it [please use this STL file](hardware/Assembly.STL)

# Firmware

- Adapt WiFi SSID and password in [config.json](firmware/data/config.json)
- Compile and flash the [firmware](firmware/) using platform.io. 

# Demo

- Start Processing and run Mountains_processing.pde
- Start OBS and share Preview output via NDI (use the plugin available at [https://github.com/Palakis/obs-ndi](https://github.com/Palakis/obs-ndi)
- In a terminal windo, run `ndi2shmdata -n "SAT-0119 (m)" -v ~/shmdata -d`, replacing *SAT-0119 (m)* by the NDI signal name (can be retrieved using `ndi2shmdata -L"
- Run Splash with the *splash_config.json* configuration file. If using the flatpak version you can run `/usr/bin/flatpak run com.gitlab.sat_metalab.Splash /PATH/TO/FILE/splash_config.json`
