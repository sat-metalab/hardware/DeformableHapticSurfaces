#include "Arduino.h"
#include "module.h"
#include <chrono>
#include "fader.h"


void Fader :: initializePins(int PIN1,int PIN2,int ENABLEPIN, int POTENTIOMETERIN, int TOUCHPIN, int PWMCHANNEL, bool ignoreTouch)
{
  
  //Assign arguments of this method to the public variables of 
  this->pin1=PIN1;
  this->pin2=PIN2;
  this->enablePin=ENABLEPIN;
  this->potentiometerPin=POTENTIOMETERIN;
  this->touchPin=TOUCHPIN;
  this->pwmChannel=PWMCHANNEL;

  //Set the pin modes
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(enablePin, OUTPUT);

  //Set the ignoreTouch argument of this method to true if you do not have a touch sensing pin
  if (ignoreTouch==false)
  {
  pinMode(touchPin, INPUT);
  }

  pinMode(potentiometerPin, INPUT);

  //Set up the pwm 
  ledcSetup(pwmChannel, FaderSignalFrequency, FaderSignalResolution);
  ledcAttachPin(enablePin, pwmChannel);


}


float Fader::getPosition()
{
  return (float(analogRead(potentiometerPin))-4095.0)/(-4095.0)*10.0;
  //maps the potentiometer reading (which is in the range 0-4095) from 0 cm to 10cm
}

void Fader::writePosition(float goal_position, float force, float sensibility)
{

  //This method writes the direction the motor needs to move to from its current position to reach the goal position within the sensibility
  //However, this method only checks what direction the motor needs to go to for the CURRENT position
  //Thus, this method needs to be looped over until the motor reaches the goal position
  //The loop has not been implemented inside the method, so that the loop doesn t block other motors from being actuated at the same time


  int goal_position_potentiometer=int(((goal_position-0.0)/(10.0-0.0))*(0.0-4095.0)+4095.0);
  // maps the positions from centimeters to potentiometer values
  int force_255=int(((force-0.0)/(100.0-0.0))*(255.0-200.0)+200.0);
  // maps the force from percentage to a range up to 255
  int sensibility_potentiometer=int(((sensibility-0.0)/(100.0-0.0))*(4095.0-0.0)+0.0);
  // maps the sensibility from percentage to potentiometer values
  int pos = analogRead(potentiometerPin);
  //Read current position of potentiometer without converting it in cm
 

  //Set the speed of each motor to the maximum possible
  ledcWrite(pwmChannel, force_255);
  
  //If not at center bring motor  to center +- sensibility
  //Motor moves up
  if (pos>goal_position_potentiometer+sensibility_potentiometer)
  {
    digitalWrite(pin1, HIGH);
    digitalWrite(pin2, LOW);
  }

  //If not at center bring motor  to center +- sensibility
  //Motor moves down
  else if (pos<goal_position_potentiometer-sensibility_potentiometer)
  {
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, HIGH);
  }

  //If at center bring motor  don t do anything
  //Motor doesn t move
  else 
  {
    digitalWrite(pin1, LOW);
    digitalWrite(pin2, LOW);
  }

 
}

void Fader::initializePositionSpeed(float goal_position, float Time)
{
 //Set up the goal position and the time required for the fader to get to that goal position (hence definining its speed)
 //Based on this, it will calculate the step size between intermediate positions
 EndPosition=goal_position;
 StartPosition=getPosition();
 Step=(EndPosition-StartPosition)/(Time);
 StepPosition=StartPosition;
 StopAnimation=false;
}

void Fader::writePositionSpeed(float force, float sensibility,int Delay)
{
  //Once you have initalized the step size using the initializePositionSpeed(float goal_position, float Time) method
  //This method writes the direction the motor needs to move to from its current position to reach each intermediate position
  //The method stops once the motor has reached its final goal position
  //However, this method only checks what direction the motor needs to go to for the CURRENT position
  //Thus, this method needs to be looped over until the motor reaches the final goal position
  //A delay of 1 ms is essential for the speed of the fader to be as expected: set the delay to 1ms inside the class method if one motor only is beeing actuated inside the loop.
  //If more than one fader are being actuated, set the methods' delay argument to 0, and put a single delay of 1ms inside the loop 
  //If you set the delay inside the methods to 1ms (instead of 0ms inside the method and 1ms inside the loop), and you actuate 4 faders, all the delays will add up: the speed of each fader will be 4 times slower)
  //The loop has not been implemented inside the method, so that the loop doesn t block other motors from being actuated at the same time

  //If you are at the end position stop the animation
  if (abs(StepPosition-EndPosition)<0.02)
    {
    StopAnimation=true;
    }

  //If you are not at the end position go to the next intermediate position
  if (StopAnimation==false)
    {
    StepPosition+=Step;
    writePosition(StepPosition,force,sensibility);
    delay(Delay);
    }

  
}


//----------------------------------------------------
//The following functions are not part of the Fader class, but are very useful
//----------------------------------------------------

void GoToFast(Fader faders[],float positions[],int timeDelay)
{
  //This functions makes all the given faders go quickly to a given position
  //The function will block anything else from happening for a delay of timeDelay in ms, this allows the motors to have time to arrive to their final positions.
  //Each fader's final goal position must be specified in positions[]
  //You CANNOT send any position messages without modifying this code. An example exists in main.cpp and is called AllGoToFast

  //Get the size of the array of Faders given as argument
  int numberFaders= *(&faders + 1) - faders;

  //Start the clock and define variables
  auto begin = std::chrono::high_resolution_clock::now();
  auto end = std::chrono::high_resolution_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
  double timePassed = elapsed.count() * 1e-6;
 
  //t is a variable that tracks how many iterations have been made
  int t=0;
  while (timePassed<timeDelay)
  {

    //write the positions
    for (int i=0;i<numberFaders;i++){faders[i].writePosition(positions[i],100,1);} //Surface starts flat
    
    //Add code here to send position message. 
    //We recommend you do not send a message every iteration because it would slow everything down, thus we recommend you use if (t%1000==0){sendMessageFunction();}

    //Update how much time has passed
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    timePassed=elapsed.count() * 1e-6;
    
    t++;
  }
}




void GoToSpeeds(Fader faders[],float positions[], int times[],int timeBetweenMessages=500){

  //This functions makes all the faders go to a given position in a given time (so all have a defined speed)
  //The function will block anything else from happening until the time when all faders SHOULD  have reached their final positions (so if one motor is physically blocked from reaching its goal position, it won t affect the blocking time)
  //Each fader's final goal position must be specified in positions[]
  //You CANNOT send any position messages without modifying this code. An example with messages exists in main.cpp and is called AllGoToSpeeds

  //Get the size of the array of Faders given as argument
  int numberFaders= *(&faders + 1) - faders;

  //Calculate the intermediate positions for each motor
  for (int i=0;i<numberFaders;i++){faders[i].initializePositionSpeed(positions[i],times[i]);}


  int t=0; //More or less counts the time in ms
  int numberMotorsStopped=0; //counts how many motors have gone through all of their steps and thus SHOULD be at their goal position
  while (numberMotorsStopped!=numberFaders)
  {
  
      for (int i=0;i<numberFaders;i++){
        //Write the position with given speed of the fader
        faders[i].writePositionSpeed(100,1,0); //0ms internal delay as the 1ms delay is at the end of this loop
        //If the fader should have reached its destination by now, count it.
        if (faders[i].StopAnimation==true){numberMotorsStopped+=1;}
        else {numberMotorsStopped=0;}
        }

      //Add code here to send position message. 
      //We recommend you do not send a message every microsecond because it would slow everything down, thus we recommend you use if (t%1000==0){sendMessageFunction();}

      //This delay is important, so that the speed are accurate
      delay(1);
      t+=1;
  }
}

//----------------------------------------------------