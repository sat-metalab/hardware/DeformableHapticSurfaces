#include "Arduino.h"
#include "module.h"
#include <chrono>
#include "fader.h"


Puara puara;

  
//Creating 4 different Fader instances 
const int numberFaders=4;
Fader faders [numberFaders]; 



void setupFaders(){
    //Set up all of your faders here so that you have a readable setup() function
    //Defining the pins
    //Serial.println("Defining the pins");
    faders[0].initializePins(15,32,25,36,12,0,true); //true:ignore the touch sensing
    faders[1].initializePins(14,22,23,39,13,1,true); //true:ignore the touch sensing
    faders[2].initializePins(17,16,21,34,27,2,true); //true:ignore the touch sensing
    faders[3].initializePins(19,18,5,33,4,3,true);   //true:ignore the touch sensing
    //Serial.println("Successfully inialized the pins");

  
}



void setup() {
    Serial.begin(115200);
    //Serial.println("Starting Puara");
    puara.start();
    //Serial.println("Puara Successfully started");
    setupFaders();
    //Serial.println("SETUP successfully finished");
 
}



void sendPositions() //Sends OSC messages of the position of every fader to the given IP
{
  //For every motor send its position and its number
  for (int i=0; i<numberFaders;i++)
  {
    std::string osc_namespace = "/height/m" + std::to_string(i);
    if (puara.oscIP1 != "0.0.0.0" || puara.oscIP1 != "") {
        lo_send(puara.lo_oscIP1, osc_namespace.c_str(), "f", faders[i].getPosition());
    }
    if (puara.oscIP2 != "0.0.0.0" || puara.oscIP2 != "") {
        lo_send(puara.lo_oscIP2, osc_namespace.c_str(), "f", faders[i].getPosition());
    }
  }

}



void allGoToFast(float positions[],int timeDelay){
  //This functions makes all the faders go quickly to a given position
  //The function will block anything else from happening for a delay of timeDelay in ms, this allows the motors to have time to arrive to their final positions.
  //Each fader's final goal position must be specified in positions[]
  //This code is a modified version of the GoToFast function of fader.cpp : in this version positions messages are sent and all faders are targeted

  //Start the clock and define variables  
  auto begin = std::chrono::high_resolution_clock::now();
  auto end = std::chrono::high_resolution_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
  double timePassed = elapsed.count() * 1e-6;

  //t is a variable that tracks how many iterations have been made
  int t=0;
  while (timePassed<timeDelay){
    //write the positions
    for (int i=0;i<numberFaders;i++){faders[i].writePosition(positions[i],100,1);} //Surface starts flat
    
    //Every 1000 iterations (allows the code not to slow down too much) send the position through OSC messages
    if (t%1000==0){sendPositions();}

    //Update the time that has passed
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    timePassed=elapsed.count() * 1e-6;

    t++;
  }
}



void allGoToSpeeds(float positions[], int times[],int timeBetweenMessages=500){

//This functions makes all the faders go to a given position in a given time (so all have a defined speed)
//The function will block anything else from happening until the time when all faders SHOULD  have reached their final positions (so if one motor is physically blocked from reaching its goal position, it won t affect the blocking time)
//Each fader's final goal position must be specified in positions[]
//This code is a modified version of the GoToFast function of fader.cpp : in this version positions messages are sent and all faders are targeted

//Calculate the intermediate positions for each motor
for (int i=0;i<numberFaders;i++){faders[i].initializePositionSpeed(positions[i],times[i]);}


int t=0; //More or less counts the time in ms
int numberMotorsStopped=0; //counts how many motors have gone through all of their steps and thus SHOULD be at their goal position
while (numberMotorsStopped!=numberFaders)
{
 
     for (int i=0;i<numberFaders;i++){
      //Write the position with given speed of the fader
      faders[i].writePositionSpeed(100,1,0); //0ms internal delay as the 1ms delay is at the end of this loop
      //If the fader should have reached its destination by now, count it.
      if (faders[i].StopAnimation==true){numberMotorsStopped+=1;}
      else {numberMotorsStopped=0;}
      }

     //Send the position through OSC messages
     if (t%timeBetweenMessages==0){sendPositions();}

     //This delay is important, so that the speed are accurate
     delay(1);
     t+=1;
}
}




//----------------------------------------------------
//The following functions are all part of the animation
//----------------------------------------------------

void resetSurface()
{
  Serial.println("Flattening : Starting position");
  float positionsA [] = {5.0,5.0,5.0,5.0};
  allGoToFast(positionsA,1500);
  Serial.println("Flat");
}




void animation1()
{
  Serial.println("Going to first configuration : Genesis of the mountains");
  float positions [] = {7.0,3.5,8.8,4.7};
  int times [] = {5000,3000,200,5000};
  allGoToSpeeds(positions,times);

  Serial.println("Adjusting");
  allGoToFast(positions,1500);
  Serial.println("Done with animation1");
}




void animation2()
{


  Serial.println("Going to second configuration : Erosion of the mountains");
  float positions [] = {5.6,3.8,4.8,4.7};
  int times [] = {15000,17000,14200,16000};
  allGoToSpeeds(positions,times);

  Serial.println("Adjusting");
  allGoToFast(positions,1500);
  Serial.println("Done with animation 2");

}



void animation3()
{


  Serial.println("Going to third configuration : Earthquake");
  float positionsA [] = {5.6,3.8,4.8,4.7};
  float positionsB [] = {4.6,4.8,5.3,4.0};
  for (int i=0;i<50;i++){
  allGoToFast(positionsA,50);
  allGoToFast(positionsB,50);
  }
  Serial.println("Done with animation 3");

}



void animation4()
{



  Serial.println("Going to fourth configuration : Volcano appears");
  float positions [] = {5.8,7.5,7.5,8.7};
  int times [] = {500,3000,200,5000};
  allGoToSpeeds(positions,times);

  Serial.println("Adjusting");
  allGoToFast(positions,1500);
  Serial.println("Done with animation4");


}



void animation5()
{



  Serial.println("Going to fifth configuration : Volcano grows");
  float positions [] = {10,10,10,10};
  int times [] = {500,500,500,500};
  allGoToSpeeds(positions,times);


  Serial.println("Adjusting");
  allGoToFast(positions,1500);
  Serial.println("Done with animation 5");


}



void animation6()
{


  Serial.println("Going to third configuration : Volcano shakes");
  float positionsA [] = {7,7,7,7};
  float positionsB [] = {7.5,7.5,7.5,7.5};
  
  for (int i=0;i<50;i++){
  allGoToFast(positionsA,50);
  allGoToFast(positionsB,50);
  }

  Serial.println("Done with animation 6");

}



void animation7()
{


  Serial.println("Going to third configuration : Volcanic eruption");
  float positionsA [] = {10,10,10,10};
  float positionsB [] = {0,3,6,0};
  allGoToFast(positionsA,100);
  allGoToFast(positionsB,500);

  Serial.println("Done with animation 7");

}



void animation8()
{


  Serial.println("Going to second configuration : Erosion of the eruption");
  float positions [] = {5,5,5,5};
  int times [] = {5000,7000,4200,6000};
  allGoToSpeeds(positions,times);

  Serial.println("Adjusting");
  allGoToFast(positions,1500);
  Serial.println("Done with animation 8");

}



void runAnimation()
{
  resetSurface();

  animation1();
  sendPositions();
  Serial.println("Waiting 10s");
  delay(1000);

  animation2();
  sendPositions();
  Serial.println("Waiting 10s");
  delay(10000);

  animation3();
  sendPositions();

  animation4();
  sendPositions();
  Serial.println("Waiting 10s");
  delay(1000);

  animation5();
  sendPositions();
  Serial.println("Waiting 10s");
  delay(1000);

  animation6();
  sendPositions();
  Serial.println("Waiting 10s");
  delay(1000);

  animation7();
  sendPositions();
  Serial.println("Waiting 10s");
  delay(1000);

  animation8();
  sendPositions();
}

//----------------------------------------------------


void loop() 
{
  
  runAnimation();
  //sendPositions(); delay(50);//Uncomment this line and the comment the above line, if you only want to send the positions of the faders, without the motors being active

}


