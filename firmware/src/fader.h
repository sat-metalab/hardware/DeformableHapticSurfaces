#ifndef FADER_H
#define FADER_H

class Fader
{
     public :
        int pin1;
        int pin2;
        int enablePin;
        int potentiometerPin;
        int touchPin;
        int pwmChannel;
        
        const int FaderSignalFrequency = 30000;
        const int FaderSignalResolution = 8;

   

        void initializePins(int PIN1,int PIN2,int ENABLEPIN, int POTENTIOMETERIN, int TOUCHPIN, int PWMCHANNEL, bool ignoreTouch=false); //Allows the user to initialize the pins of the esp32 to which the fader is connected
        float getPosition(); //Returns the current position of the fader in cm.
        void writePosition(float goal_position, float force, float sensibility); //Allows the user to write a position to the fader. This function must be looped until fader adjusts its actual current position to the goal position. Does not allow for direct speed control.
        void initializePositionSpeed(float goal_position, float Time); //The user must call this function to set up a speed control
        void writePositionSpeed(float force, float sensibility,int Delay=0); //The user must loop this function until StopAnimation is true. There should be a delay of 1 millisecond in between each iteration. This function will move the fader from its current actual position to the target position in the Time given in milli seconds.

    
        float EndPosition;
        float StepPosition;
        float StartPosition;
        float Step;
        bool StopAnimation=false;

};
       
#endif