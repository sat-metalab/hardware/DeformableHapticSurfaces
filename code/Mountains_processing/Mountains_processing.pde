/* DeformableHapticSurfaces
*  Demo (visual mountains)
*/

import oscP5.*;
import netP5.*;

OscP5 osc;

int screenX = 1280;
int screenY = 800;
float max_size = 800;

Mountain m1 = new Mountain(screenX, screenY, 0.69, 0.33, max_size);
Mountain m2 = new Mountain(screenX, screenY, 0.69, 0.69, max_size);
Mountain m3 = new Mountain(screenX, screenY, 0.33, 0.69, max_size);
Mountain m4 = new Mountain(screenX, screenY, 0.33, 0.33, max_size);

void settings() {
  //fullScreen();
  size(screenX, screenY);
}

void setup() {
  
  /* start oscP5, listening for incoming messages at port 12000 */
  osc = new OscP5(this,12000);

  background(34,139,34); // forestgreen
}

void draw() { 
background(34,139,34);
m1.draw_mountain();
m2.draw_mountain();
m3.draw_mountain();
m4.draw_mountain();

}

class Mountain {
    float size = 300;
    float sea_treshold = 0;
    float dirt_treshold = 0.1;
    float forest_treshold = 0;
    float snow_treshold = 0.4;
    float sea;
    float dirt;
    float forest;
    float snow;
    float posX;
    float posY;
    int resX;
    int resY;
    int[] background = {34,139,34}; // forestgreen

    Mountain (int rX, int rY, float pX, float pY, float sz, float st, float dt, float ft, float snt) {
        resX = rX;
        resY = rY;
        posX = pX;
        posY = pY;
        size = sz;
        sea_treshold = st;
        dirt_treshold = dt;
        forest_treshold = ft;
        snow_treshold = snt;
    }
    
    Mountain (int rX, int rY, float pX, float pY, float sz) {
        resX = rX;
        resY = rY;
        posX = pX;
        posY = pY;
        size = sz;
    }

    void update(float height) { // height between -1 and 1
        // calculate sea
        if (height >= sea_treshold){
            sea = 0;
        } else {
            sea = abs(height);
        }
        // calculate dirt
        if (height <= dirt_treshold){
            dirt = 0;
        } else {
            dirt = map(height,dirt_treshold,1,0,0.8);
        }
        // calculate forest
        if (height <= forest_treshold){
            forest = 0;
        } else {
            forest = height;
        }
        // calculate snow
        if (height <= snow_treshold){
            snow = 0;
        } else {
            snow = map(height,snow_treshold,1,0,0.5);
        }
    }

    void draw_mountain() {
        stroke(0,0,0,0); // set stroke to black and transparent
        fill(0,105,148); // Sea blue
        circle(resX*posX, resY*posY, size * sea);
        //fill(34,139,34); //forestgreen
        //circle(resX*posX, resY*posY, size * forest);
        fill(124,94,66); // dirt
        circle(resX*posX, resY*posY, size * dirt);
        fill(200,198,198); // snow
        circle(resX*posX, resY*posY, size * snow);
    }
}

/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/height/m0")) {
    m1.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1)); 
  }
  if (theOscMessage.checkAddrPattern("/height/m1")) {
    m2.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1)); 
  }
  if (theOscMessage.checkAddrPattern("/height/m2")) {
    m3.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1)); 
  }
  if (theOscMessage.checkAddrPattern("/height/m3")) {
    m4.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1)); 
  }
  if (theOscMessage.checkAddrPattern("/height")) {
    m1.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1));
    m2.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1));
    m3.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1));
    m4.update(map(theOscMessage.get(0).floatValue(),0,10,-1,1));
  }
}
